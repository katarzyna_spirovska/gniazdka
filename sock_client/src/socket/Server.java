package socket;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {

    private static final int PORT = 6789;
    private static final int X = 1;
    private static final int O = 2;
    private ServerSocket serverSocket;
    private Socket client1;
    private Socket client2;

    public Server() {
        try {
            serverSocket = new ServerSocket(PORT);
            collectClients();
            game();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void collectClients() throws IOException {
        client1 = serverSocket.accept();
        client2 = serverSocket.accept();
    }

    void game() throws IOException {

        sendComplete(client1);
        sendComplete(client2);

        sendSymbol(client1, X);
        sendSymbol(client2, O);

        Socket from = client1;
        Socket to = client2;

        while (step(from, to)) {
            Socket tmp = from;
            from = to;
            to = tmp;
        }

    }

    void sendComplete(Socket client) throws IOException {
        client.getOutputStream().write(Codes.COMPLETE);
    }

    void sendSymbol(Socket client, int symbol) throws IOException {
        client.getOutputStream().write(Codes.SETSYMBOL);
        client.getOutputStream().write(symbol);
    }

    boolean step(Socket sender, Socket receiver) throws IOException {
        int read = sender.getInputStream().read();
        if(read == Codes.FINISH){
            sender.close();
            receiver.close();
            //sender.getOutputStream().write(read);
            //receiver.getOutputStream().write(read);
            return false;
        }
        int field = sender.getInputStream().read();
        receiver.getOutputStream().write(Codes.SETSTEP);
        receiver.getOutputStream().write(field);
        return true;
    }

    public static void main(String[] args) {
         new Server();
    }
}

