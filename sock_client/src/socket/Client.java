package socket;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class Client extends JFrame {

    private static final long serialVersionUID = 1L;

    private static final int numberOfColumns = 3;

    private JFrame frame = new JFrame("Client");
    private JPanel gamePanel;
    private JButton[][] button = new JButton[numberOfColumns][numberOfColumns];
    private int[][] stateArray = new int[numberOfColumns][numberOfColumns]; // 0 - not selected, 1 - x, -1 = o
    private JLabel labelInfo;

    private boolean turn;
    private static final int X = 1;
    private static final int O = -1;
    private int symbol; // 1 - x, -1 - o
    private int count;

    private ImageIcon xIcon = new ImageIcon("Meow.png");
    private ImageIcon oIcon = new ImageIcon("Young_Boar.png");

    class GameConnection {
        private Socket socket;
        private static final int PORT = 6789;

        void start() {
            try {
                socket = new Socket("127.0.0.1", PORT);
                setLabelInfo("Po��czono z serwerem");
                game();
            } catch (IOException e) {
                setLabelInfo("Nie uda�o si� po��czy� z serwerem");
                e.printStackTrace();
            }
        }

        void game() throws IOException {
            while (socket.isConnected()) {
                int read = socket.getInputStream().read();
                switch (read) {
                    case Codes.FINISH:
                        socket.close();
                        break;
                    case Codes.COMPLETE:
                        setLabelInfo("Po��czono z innym klientem");
                        break;
                    case Codes.SETSYMBOL: {
                        symbol = socket.getInputStream().read();
                        symbol = (symbol > 1 ? O : X);
                        System.out.println(symbol);
                        turn = (symbol == X);

                        for (int i = 0; i < numberOfColumns * numberOfColumns; i++) {
                            button[i / numberOfColumns][i % numberOfColumns].setEnabled(turn);
                        }

                        String message = (turn ? "Zacznij gr�" : "Ruch przeciwnika");
                        setLabelInfo(message);
                    } break;
                    case Codes.SETSTEP: {
                        int step = socket.getInputStream().read();
                        step(step / numberOfColumns, step % numberOfColumns, symbol * -1);
                    } break;
                }
            }
        }

        void writeStep(int x, int y) {
            try {
                socket.getOutputStream().write(Codes.SETSTEP);
                socket.getOutputStream().write(x * numberOfColumns + y);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        void writeFinish() {
            try {
                socket.getOutputStream().write(Codes.FINISH);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private GameConnection gc = new GameConnection();

    public Client() throws Exception {
        //GUI
        makeGuiGreatAgain();

        new Runnable() {
            @Override
            public void run() {
                gc.start();
            }
        }.run();
    }

    private void makeGuiGreatAgain() {
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.setSize(300, 390);
        frame.setLayout(new BorderLayout());
        frame.setVisible(true);
        frame.setResizable(false);

        JPanel infoPanel = new JPanel(new BorderLayout());
        gamePanel = new JPanel(new GridLayout(numberOfColumns, numberOfColumns));

        infoPanel.setPreferredSize(new Dimension(300, 50));
        gamePanel.setPreferredSize(new Dimension(300, 300));

        gamePanel.setBackground(Color.gray);
        labelInfo = new JLabel("Info", SwingConstants.CENTER);
        infoPanel.add(labelInfo);
        infoPanel.setBackground(Color.lightGray);

        for (int i = 0; i < numberOfColumns * numberOfColumns; i++) {
            final int consti = i;
            JButton current = new JButton("");
            button[i / numberOfColumns][i % numberOfColumns] = current;
            current.setVisible(true);
            current.setBackground(Color.white);
            gamePanel.add(current);
            current.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    step(consti / numberOfColumns, consti % numberOfColumns, symbol);

                    System.out.println(consti);
                }
            });
        }
        frame.add(infoPanel, BorderLayout.NORTH);
        frame.add(gamePanel, BorderLayout.SOUTH);
        frame.revalidate();
        frame.repaint();
    }

    void step(int x, int y, int symbol) {
        count++;

        if (stateArray[x][y] != 0) {
            System.out.println("Zaj�te pole");
        }

        if (symbol == this.symbol) {
            gc.writeStep(x, y);
        }

        stateArray[x][y] = symbol;
        button[x][y].setEnabled(false);
        //button[x][y].setText((symbol == X ? "X" : "O"));
        button[x][y].setIcon((symbol == X ? xIcon : oIcon));

        if (count == 9) {
            setLabelInfo("Pat");
            gc.writeFinish();
            return;
        }

        if (checkIfWin()) {
            String text = (symbol == this.symbol ? "Wygra�e�" : "Przegra�e�");
            setLabelInfo(text);

            for (int i = 0; i < numberOfColumns * numberOfColumns; i++) {
                button[i / numberOfColumns][i % numberOfColumns].setEnabled(false);
            }

            if (symbol == this.symbol) {
                gc.writeFinish();
            }
            return;
        }

        turn = !turn;

        for (int i = 0; i < numberOfColumns * numberOfColumns; i++) {
            boolean set = (stateArray[i / numberOfColumns][i % numberOfColumns] != 0);
            button[i / numberOfColumns][i % numberOfColumns].setEnabled(!set && turn);
        }

        String text = (turn ? "Twoja tura" : "Tura przeciwnika");
        setLabelInfo(text);
    }

    boolean checkIfWin() {
        for (int i = 0; i < numberOfColumns; i++) {
            int vertical = 0;
            int horizontal = 0;

            for (int j = 0; j < numberOfColumns; j++) {
                vertical += stateArray[i][j];
                horizontal += stateArray[j][i];
            }

            if (Math.abs(vertical) == numberOfColumns || Math.abs(horizontal) == numberOfColumns) {
                return true;
            }
        }

        int leftToRight = 0;
        int rightToLeft = 0;

        for (int i = 0; i < numberOfColumns; i++) {
            int j = numberOfColumns - i - 1;

            leftToRight += stateArray[i][i];
            rightToLeft += stateArray[i][j];
        }

        return Math.abs(leftToRight) == numberOfColumns || Math.abs(rightToLeft) == numberOfColumns;
    }

    void setLabelInfo(String text) {
        labelInfo.setText(text);
    }

    public static void main(String[] args) throws Exception {
        new Client();
    }
}
